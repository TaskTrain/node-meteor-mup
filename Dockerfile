# Base on current Ubuntu LTS https://hub.docker.com/_/ubuntu
FROM ubuntu:20.04

LABEL maintainer="Keith Gillette <Keith.Gillette@TaskTrain.app>"

# Prevent `apt-get` service restart prompt which hangs `docker build`
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# Install latest OS Updates
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils

# Install necessary additional packages; Do we need apache2 & git?
RUN apt-get update && apt-get install -y openssh-server apache2 supervisor git curl awscli nodejs

# Setup directories for some reason
RUN mkdir -p /var/lock/apache2 /var/run/apache2 /var/run/sshd /var/log/supervisor

# Configure Supervisor process control system
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Open SSH & HTTP ports
EXPOSE 22 80
CMD ["/usr/bin/supervisord"]

# Setup AWS CLI Command Completion
RUN echo complete -C '/usr/local/bin/aws_completer' aws >> ~/.bashrc

# Install & configure Meteor <https://www.meteor.com/>
ENV METEOR_VERSION 2.7.2
RUN curl https://install.meteor.com/?release=$METEOR_VERSION -o /tmp/install_meteor.sh
RUN sh /tmp/install_meteor.sh
ENV METEOR_ALLOW_SUPERUSER true

# Configure APT package manager to access newer Node.js versions
ENV NODE_VERSION_MAJOR 14
RUN curl -sL https://deb.nodesource.com/setup_$NODE_VERSION_MAJOR.x | bash
# Install Node.js + Node Package Manager (NPM) to allow MUP installation
RUN apt-get update && apt-get install -y nodejs
# Install Node's version manager
RUN npm install -g n
# Update Node to match Meteor's internal version
ENV NODE_VERSION 14.19.1
RUN n $NODE_VERSION
# Update NPM to match Meteor's internal version
ENV NPM_VERSION 6.14.16
RUN npm install -g npm@$NPM_VERSION

# Install Meteor Up (MUP) <http://meteor-up.com/>
ENV MUP_VERSION 1.5.8
RUN npm install -g mup@$MUP_VERSION
